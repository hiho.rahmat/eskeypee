extends Node2D

func _ready():
	self.visible = true
	
func _on_Area2D_body_entered(body):
	self.visible = false


func _on_Area2D_body_exited(body):
	self.visible = true
