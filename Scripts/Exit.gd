extends Area2D


export var keysNeeded=0
export (String) var sceneName

func _on_Exit_body_entered(body):
	if body.get("TYPE")=="player" and Global.keys>=keysNeeded:
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
		Global.keys =0
