extends Label

export var minute = 0
export var second = 0

export var sceneName = ""

func _process(delta):
	if second<0 :
		minute-=1
		second=60
	
	if minute<0:
		Global.lives -=1
		Global.keys =0
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
	
	if (Global.lives == 0):
			get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
			Global.lives = 3
			
	set_text("Time "+str(minute)+":"+str(second))



func _on_Timer_timeout():
	second-=1
