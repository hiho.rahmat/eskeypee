extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)
const TYPE = "player"

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var idle = self.get_node("Idle")
onready var walk = self.get_node("Walk")
onready var jump = self.get_node("Jump")
onready var fall = self.get_node("Fall")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		velocity.y = jump_speed
	elif Input.is_action_pressed("ui_right"):
		velocity.x += speed
	elif Input.is_action_pressed("ui_left"):
		velocity.x -= speed
	else :
		velocity.x = 0

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		idle.hide()
		walk.hide()
		
		if velocity.x >= 0:
			idle.flip_h = false
			walk.flip_h = false
			jump.flip_h = false
			fall.flip_h = false
			
		else:
			idle.flip_h = true
			walk.flip_h = true
			jump.flip_h = true
			fall.flip_h = true
		
		
		if velocity.y<0:
			jump.show()
			fall.hide()
			animator.play("Jump")
		else:
			jump.hide()
			fall.show()
			animator.play("Fall")
		
		
	elif velocity.x != 0:
		idle.hide()
		walk.show()
		jump.hide()
		fall.hide()
		animator.play("Walk")
		if velocity.x >= 0:
			idle.flip_h = false
			walk.flip_h = false
			jump.flip_h = false
		else:
			idle.flip_h = true
			walk.flip_h = true
			jump.flip_h = true
	else:
		animator.play("Idle")
		idle.show()
		walk.hide()
		jump.hide()
		fall.hide()
		
	
