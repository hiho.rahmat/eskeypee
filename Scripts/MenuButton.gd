extends LinkButton

export(String) var scene_to_load

func _on_NewGame_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_SelectStage_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Stage_1_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_BackToMainMenu_pressed():
	Global.lives=3
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
